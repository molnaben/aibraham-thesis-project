import { AppBar, Toolbar, Typography } from '@mui/material'
import React from 'react'

export default function LogoBar() {
    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant='h6'>Aibraham</Typography>
            </Toolbar>
        </AppBar>
    )
}
